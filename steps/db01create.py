import MySQLdb

user = 'root'
password = '1My_sql!SQL'

db = MySQLdb.connect(user=user, password=password)

### DDL (data definition language)
create_database = """CREATE DATABASE IF NOT EXISTS music;USE music;"""
create_tables = """
CREATE TABLE instruments(
    instrument_id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    name VARCHAR(30) NOT NULL,
    family VARCHAR(30) NOT NULL,
    difficulty ENUM('easy', 'medium', 'hard') NOT NULL
);
"""

### CREATE UNIQUE INDEX `uniq_instruments_name_family_difficulty`
### ON `music`.`instruments` (name, family, difficulty);

with db:
    cursor = db.cursor()

    try:

        cursor.execute(create_database)
        cursor.execute(create_tables)

    ### Handle exception
    except Exception as error:

        print( error )
