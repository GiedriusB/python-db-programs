from sqlalchemy import create_engine, ForeignKey
from sqlalchemy.exc import IntegrityError, PendingRollbackError
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, String, Integer
from sqlalchemy.orm import sessionmaker, relationship
from sqlalchemy import select, and_, between, join

user = 'root'
password = '1My_sql!SQL'

eng = create_engine('mysql://' + user + ':' + password + '@localhost:3306/cinematic')
base = declarative_base()

class Directors(base):
    __tablename__ = 'directors'

    director_id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(45), nullable=False)
    surname = Column(String(45), nullable=False)
    rating = Column(Integer, nullable=False)

    ### Ensure that referencing columns are associated with a ForeignKey or ForeignKeyConstraint
    ###
    ### ALTER TABLE cinematic.movies ADD FOREIGN KEY (director_id) REFERENCES directors(director_id);
    ###

    movies = relationship('Movies', back_populates='directors', lazy='dynamic', cascade="all, delete",
                          primaryjoin="Movies.director_id == Directors.director_id", passive_deletes=True)

    # Custom (generic) constructor
    # REF: https://www.digitalocean.com/community/tutorials/how-to-use-args-and-kwargs-in-python-3
    def __init__(self, *args, **kwargs):
        try:
            for key, value in kwargs.items():

                # args -- tuple of anonymous arguments
                # kwargs -- dictionary of named arguments
                self.name = kwargs.get('name', value)
                self.surname = kwargs.get('surname', value)
                self.rating = kwargs.get('rating', value)

        except KeyError as error:
            print( "Not found in DB dict description %s" % error )

    def __init__(self, name, surname, rating):
        self.name = name
        self.surname = surname
        self.rating = rating

    def __repr__(self):
        return f'<Directors: id={self.director_id}, name={self.name}, surname={self.surname}, rating={self.rating}>'\
               f" OR as JSON:\n" + self.jsonize()

    def as_dict(self):
        return {c.name: getattr(self, c.name) for c in self.__table__.columns}

    def jsonize(self):
        variables = [var for var in self.as_dict()]
        #fl = filter(lambda x: type( x ), variables );

        return "{" + ",".join([f"\"{var}\": \"{getattr(self, var)}\"" for var in variables]) + "}"

class Movies(base):
    __tablename__ = 'movies'

    movie_id = Column(Integer, primary_key=True, autoincrement=True)
    title = Column(String(45), nullable=False)
    year = Column(Integer, nullable=False)
    category = Column(String(45), nullable=False)

    ### Ensure that referencing columns are associated with a ForeignKey or ForeignKeyConstraint
    director_id = Column(Integer, ForeignKey('directors.director_id'), nullable=False)
    rating = Column('score', Integer, nullable=False)

    directors = relationship('Directors', back_populates='movies')

    # Custom constructor
    def __init__(self, db_dict):
        try:

            self.title = db_dict['title']
            self.year = db_dict['year']
            self.category = db_dict['category']
            self.director_id = db_dict['director_id']
            self.rating = db_dict['rating']

        except KeyError as error:
            print( "Not found in DB dict description %s" % error )

    def __repr__(self):
        return f"<Movies: movie_id={self.movie_id}, title={self.title}, year={self.year}, " \
               f"category={self.category}, director_id={self.director_id}, rating={self.rating}>"

base.metadata.create_all(eng)
Session = sessionmaker(bind=eng)
session = Session()

try:
    session.commit()

# Exception handling for hardcoded input
except IntegrityError as error:
    print("Possible DB duplicate: %s" % error)

except PendingRollbackError as roll_back_error:
    print("Rollback is due: %s" % roll_back_error)

session = Session()

try:
    session.commit()

# Exception handling for file input
except IntegrityError as error:
    print("Possible DB duplicate: %s" % error)

except PendingRollbackError as roll_back_error:
    print("Rollback is due: %s" % roll_back_error)

# Exception handling
except IntegrityError as error:
    print("Possible DB duplicate: %s" % error)

except PendingRollbackError as roll_back_error:
    print("Rollback is due: %s" % roll_back_error)

for t in base.metadata.sorted_tables:
    print(t.name)

conn = eng.connect()

print('''  ##### TASK1 ##### ''')

j = join(Directors, Movies)
select = select([Directors.name, Directors.surname]).select_from(j).where(
            and_(between(Movies.year, 2011, 2014), Movies.rating < 9))

result = conn.execute(select)

print(result.fetchall())
conn.close()

join_result = session.query(Directors.name, Directors.surname).join(Movies).filter(
            and_(between(Movies.year, 2011, 2014), Movies.rating < 9)).all()

print( "RESULT of JOIN: %s" % join_result)

try:
    for directors in result.all():
        print(directors)

    for movies in session.query(Movies).all():
        print(movies)

    conn.close()

except PendingRollbackError as roll_back_error:
    print("Program rollback failure: %s" % roll_back_error)