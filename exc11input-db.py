from sqlalchemy import create_engine
from sqlalchemy.exc import IntegrityError, PendingRollbackError
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, String, Integer
from sqlalchemy.orm import sessionmaker

base = declarative_base()
user = 'root'
password = '1My_sql!SQL'
eng = create_engine('mysql://' + user + ':' + password + '@localhost:3306/cinematic')


class Directors(base):
    __tablename__ = 'directors'

    director_id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(45), nullable=False)
    surname = Column(String(45), nullable=False)
    rating = Column(Integer, nullable=False)

    def __init__(self, name, surname, rating):
        self.name = name
        self.surname = surname
        self.rating = rating

    def __repr__(self):
        return f"<Directors: id={self.director_id}, name={self.name}, surname={self.surname}, rating={self.rating}>"


class Movies(base):
    __tablename__ = 'movies'

    movie_id = Column(Integer, primary_key=True, autoincrement=True)
    title = Column(String(45), nullable=False)
    year = Column(Integer, nullable=False)
    category = Column(String(45), nullable=False)
    director_id = Column(Integer, nullable=False)
    rating = Column(Integer, nullable=False)

    # Custom constructor
    def __init__(self, db_dict):
        try:

            self.title = db_dict['title']
            self.year = db_dict['year']
            self.category = db_dict['category']
            self.director_id = db_dict['director_id']
            self.rating = db_dict['rating']

        except KeyError as error:
            print( "Not found in DB dict description %s" % error )

    def __repr__(self):
        return f"<Movies: movie_id={self.movie_id}, title={self.title}, year={self.year}, " \
               f"category={self.category}, director_id={self.director_id}, rating={self.rating}>"

# pasirenkama lentele duomenu bazeje cinematic
while True:
        x = input('Pasirinkite duomenų bazę: 1-directors, 2-movies: ')
        if x == '1' or x == '2':
            break
        else:
            print('Ups!  Nepavyko.  Bandykite iš naujo...')

# ikelia duomenis i directors lentele
if x == '1':
    vardas = str(input('Įveskite vardą: '))
    pavarde = str(input('Įveskite pavardę: '))
    while True:
        try:
            ivert = int(input('Įvertinkite nuo 1 iki 10: '))
            if ivert >= 1 and ivert <= 10:
                break
            else:
                print('Ups!  Nepavyko.  Bandykite iš naujo...')
        except ValueError:
            print('Ups!  Nepavyko.  Bandykite iš naujo...')

    director = {'name': vardas, 'surname': pavarde, 'rating': ivert}

    base.metadata.create_all(eng)
    Session = sessionmaker(bind=eng)
    session = Session()

    rec_director = Directors(director['name'], director['surname'], director['rating'])
    print('Duomenys įrašyti į cinematic buomenų bazės directors lentelę:', rec_director)
    session.add(rec_director)

    try:
        session.commit()

    # Exception handling
    except IntegrityError as error:
        print("Possible DB duplicate: %s" % error)

    except PendingRollbackError as roll_back_error:
        print("Rollback is due: %s" % roll_back_error)

# ikelia duomenis i movies lentele
if x == '2':
    pavad = str(input('Įveskite filmo pavadinimą: '))
    while True:
        try:
            metai = int(input('Įveskite pastatymo metus: '))
            break
        except ValueError:
            print('Ups!  Nepavyko.  Bandykite iš naujo...')
    kateg = str(input('Įveskite filmo žanrą: '))
    while True:
        try:
            id = int(input('Įveskite režisieriaus id numerį: '))
            break
        except ValueError:
            print('Ups!  Nepavyko.  Bandykite iš naujo...')
    while True:
        try:
            ivert = int(input('Įvertinkite nuo 1 iki 10: '))
            if ivert >= 1 and ivert <= 10:
                break
            else:
                print('Ups!  Nepavyko.  Bandykite iš naujo...')
        except ValueError:
            print('Ups!  Nepavyko.  Bandykite iš naujo...')



    movie = {'title': pavad, 'year': metai, 'category': kateg, 'director_id': id, 'rating': ivert}
    
    base.metadata.create_all(eng)
    Session = sessionmaker(bind=eng)
    session = Session()

    rec_movie = Movies(movie)
    print('Duomenys įrašyti į cinematic buomenų bazės movies lentelę:', rec_movie)
    session.add(rec_movie)

    try:
        session.commit()

    # Exception handling
    except IntegrityError as error:
        print("Possible DB duplicate: %s" % error)

    except PendingRollbackError as roll_back_error:
        print("Rollback is due: %s" % roll_back_error)
