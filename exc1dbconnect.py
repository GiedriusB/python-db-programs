# Works only for Python2.7

# https://mysqlclient.readthedocs.io/user_guide.html
import pymysql as MySQLdb

from sqlalchemy import create_engine
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, String, Integer, Date

user = 'root'
password = '1My_sql!SQL'

db = MySQLdb.connect(host="localhost",    # your host, usually localhost
                     user=user,           # your username
                     passwd=password      # your password
                     ) # name of the data base

cursor = db.cursor()
print( cursor )

cursor.execute("SHOW DATABASES")
data = cursor.fetchall()
print(data)
#help(data);

db = MySQLdb.connect(host="localhost",    # your host, usually localhost
                     user=user,           # your username
                     passwd=password,     # your password
                     db="car_rental" )    # name of the data base

# you must create a Cursor object. It will let
#  you execute all the queries you need
cursor = db.cursor()

# execute SQL query using execute() method.
cursor.execute("SELECT VERSION()")

# Fetch a single row using fetchone() method.
data = cursor.fetchone()
print( "Database version : %s " % data)
#help(data);

# Use all the SQL you like
cursor.execute("SELECT * FROM cars")

# print all the first cell of all the rows
for row in cursor.fetchall():

    # Syntax for all in tuple
    print( row[:] )

    # Syntax for all in tuple
    print( row[1:3] )

select_days = "SELECT STR_TO_DATE( CURDATE( ), '%Y-%m-%d' ) FROM DUAL"
cursor.execute( select_days )
data = cursor.fetchone()

print( "TO_DAYS: %s " % data)

# Free the DB connection resource
db.close()

eng = create_engine('mysql://' + user + ':' + password + '@localhost:3306/car_rental')
#print( eng )
#help( eng )

base = declarative_base()
print( base )
#help ( base )

class Cars(base):
    __tablename__ = 'cars'

    car_id = Column(Integer, primary_key=True, autoincrement=True)
    producer = Column(String(30), nullable=False)
    model = Column(String(30), nullable=False)
    year = Column(Integer, nullable=False)
    horse_power = Column(Integer, nullable=False)
    price_per_day = Column(Integer, nullable=False)

    def __repr__(self):
       return f"<Car: id={self.car_id}, producer={self.producer}, model={self.model}, year={self.year}, " \
              f"horse_power={self.horse_power}, price_per_day={self.price_per_day}>"

class Clients(base):
    __tablename__ = 'clients'

    client_id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(30), nullable=False)
    surname = Column(String(30), nullable=False)
    address = Column(String(30), nullable=False)
    city = Column(Integer, nullable=False)

    def __repr__(self):
        return f"<Clients: id={self.client_id}, name={self.name}, surname={self.surname}, " \
               f"address={self.address}, city={self.city}, klaida={self.city}>"

class Bookings(base):
    __tablename__ = 'bookings'

    booking_id = Column(Integer, primary_key=True, autoincrement=True)
    client_id = Column(Integer, nullable=False)
    car_id = Column(Integer, nullable=False)
    start_date = Column(Date, nullable=False)
    end_date = Column(Date, nullable=False)
    total_amount = Column(Integer, nullable=False)

    def __repr__(self):
        return f"<Booking: id={self.booking_id}, client_id={self.client_id}, car_id={self.car_id}, " \
               "start_date={self.start_date}, end_date={self.end_date}, total_amount={self.total_amount}>"

base.metadata.create_all(eng)

for t in base.metadata.sorted_tables:
    print(t.name)

from sqlalchemy.orm import sessionmaker

Session = sessionmaker(bind=eng)
session = Session()
client_1 = Clients(name='Jan', surname='Kowalski', address='ul. Florianska 12', city='Krakow')
car_1 = Cars(producer='Seat', model='Leon', year=2016, horse_power=80, price_per_day=200)

session.add(client_1)
session.add(car_1)
session.commit()

Session = sessionmaker(bind=eng)
session = Session()

for client in session.query(Clients).all():
    print(client)

for car in session.query(Cars).all():
    print(car)