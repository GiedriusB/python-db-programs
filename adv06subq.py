import sqlalchemy.orm
from sqlalchemy.orm import relationship, Session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, String, Integer, Date, ForeignKey, column

import logging

eng = sqlalchemy.create_engine('mysql://root:1My_sql!SQL@localhost:3306/cinematic')
print(eng)
logging.info( eng )

base = declarative_base()
conn = eng.connect()

Session = sessionmaker(bind=eng, autocommit=True)

class Directors(base):
    __tablename__ = 'directors'

    director_id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(30), nullable=False)
    surname = Column(String(30), nullable=False)
    rating = Column('score', Integer, nullable=False)
    movies = relationship('Movies', back_populates='directors', cascade="all, delete", passive_deletes=True)

    def __repr__(self):
       return f"<Directors: director_id=%d :: name=%s :: surname=%s :: rating=%d>" \
              % ( self.director_id, self.name, self.surname, self.rating )

class Movies(base):
    __tablename__ = 'movies'

    movie_id = Column(Integer, primary_key=True, autoincrement=True)
    director_id = Column(Integer, ForeignKey('directors.director_id', ondelete="CASCADE"), nullable=False)
    title = Column(String(30), nullable=False)
    year = Column(Integer, nullable=False)
    category = Column(String(10), nullable=False)
    rating = Column('score', Integer, nullable=False)
    directors = relationship('Directors', back_populates='movies')

    def __repr__(self):
       return f"<Movies: movie_id=%d :: title=%s :: year=%d :: category=%s :: director_id=%d :: rating=%d>" \
              % ( self.movie_id, self.title, self.year, self.category, self.director_id, self.rating )

try:

    session = Session()

    #### MAKE a SUB-QUERY ####
    #
    # SELECT movies.director_id FROM movies
    # WHERE movies.year < :year_1 AND movies.title LIKE :title_1
    #
    from sqlalchemy import and_
    from sqlalchemy import select, join, func

    print('''  ##### TASK8 ##### ''')

    with session.begin():

        ## NOTE change in 'scalar_subquery'
        subq = session.query(Directors.director_id).filter(Directors.name == 'Frank').scalar_subquery()

        print("\n %s\n" % subq)

        query = session.query(Movies).filter(Movies.director_id.in_( subq ));

        print("\n %s\n" % query )

        query.delete(synchronize_session='fetch')

        ### About to delete directors? - not needed! - the effect of CASCADE DELETE
        #session.query(Directors).filter(Directors.name == 'Frank').delete()

    ### CHECK the update:
    result = session.query(Directors)
    for director in result.all():
        print( "%s" % director )

    ### CHECK the update:
    result = session.query(Movies)
    for movie in result.all():
        print("%s" % movie)

    conn.close()

except Exception as e:

    print (e);