import logging
from operator import and_

import mysql.connector

import traceback
import sys

from sqlalchemy.orm import sessionmaker
from sqlalchemy import create_engine
from sqlalchemy.orm import relationship
from sqlalchemy import text, bindparam

from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, String, Integer, Date, ForeignKey, MetaData

user = 'root'
password = '1My_sql!SQL'

db = mysql.connector.connect(host="localhost",
                             user= user,
                             password= password,
                             database= "cinematic" )

directors = [ ( 'Darabont', 'Frank', 7),
              ( 'Coppola', 'Francis Ford', 8),
              ( 'Tarantino', 'Quentin', 10),
              ( 'Nolan', 'Christopher', 9),
              ( 'Fincher', 'David', 7) ]

shitf_ids = 0
movies = [ ('The Shawshank Redemption', 1994, 'Drama', shitf_ids+1, 8),
            ('The Green Mile', 1999, 'Drama', shitf_ids+1, 6),
            ('The Godfather', 1972, 'Crime', shitf_ids+2, 7),
            ('The Godfather III', 1990, 'Crime', shitf_ids+2, 6),
            ('Pulp Fiction', 1994, 'Crime', shitf_ids+3, 9),
            ('Inglourious Basterds', 2009, 'War', shitf_ids+3, 8),
            ('The Dark Knight', 2008, 'Action', shitf_ids+4, 9),
            ('Interstellar', 2014, 'Sci-fi', shitf_ids+4, 8),
            ('The Prestige', 2006, 'Drama', shitf_ids+4, 10),
            ('Fight Club', 1999, 'Drama', shitf_ids+5, 7),
            ('Zodiac', 2007, 'Crime', shitf_ids+5, 5),
            ('Seven', 1995, 'Drama', shitf_ids+5, 8),
            ('Alien 3', 1992, 'Horror', shitf_ids+5, 5)]

logger = logging.getLogger('errorlog')
hdlr = logging.FileHandler('errors.log')
formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
hdlr.setFormatter(formatter)
logger.addHandler(hdlr)
logger.setLevel(logging.INFO)

#
# CREATE TABLE IF NOT EXISTS directors (
#     director_id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
#     name VARCHAR(30) NOT NULL,
#     surname VARCHAR(30) NOT NULL,
#     score INT
#     );

# If you choose to switch the RDBMS implementation, just replace one line!
#eng = create_engine('oracle://root:1My_sql!SQL@localhost:9932/car_rental', echo='debug' )
eng = create_engine('mysql+pymysql://root:1My_sql!SQL@localhost:3306/cinematic')

base = declarative_base()
base.metadata.create_all(eng)
Session = sessionmaker(bind=eng)
session = Session()

# Define as a collection of statements.
create_db_tables = [
    "DROP TABLE IF EXISTS movies",
    "DROP TABLE IF EXISTS directors",
    """CREATE TABLE IF NOT EXISTS directors (
        director_id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
        name VARCHAR(30) NOT NULL,
        surname VARCHAR(30) NOT NULL,
        score INT
        )""",
    """CREATE TABLE IF NOT EXISTS movies (
    movie_id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
    title VARCHAR(30), 
    year INT, 
    category VARCHAR(10), 
    director_id INT, 
    score INT,
      FOREIGN KEY (director_id) REFERENCES directors(director_id)
    )"""]

def truncate_db(engine):
    # delete all table data (but keep tables)
    # we do cleanup before test 'cause if previous test errored,
    # DB can contain dust
    meta = MetaData(bind=engine)
    con = engine.connect()
    trans = con.begin()

    # Initialize DB in all statements
    cursor = db.cursor()

    # Note 'multi=True' when calling cursor.execute()
    for result in cursor.execute(';'.join(create_db_tables), multi=True):
        print (result)

    meta.reflect(bind=engine)
    for table in meta.sorted_tables:
        con.execute(f'ALTER TABLE {table.name} DISABLE KEYS;')
        print (f'TRUNC DB TABLE {table.name}')
        con.execute(table.delete())
        con.execute(f'ALTER TABLE {table.name} ENABLE KEYS;')
    trans.commit()

def insert_directors(connection, directors):
    if not directors:
        return
    insert_sql = """
    INSERT INTO directors (surname,name,score) VALUES(%s, %s, %s)"""
    connection.cursor().executemany(insert_sql, directors)

    print('DB OK: ' + str(directors) + " executed.")

    connection.commit()

def insert_movies(connection, movies):
    if not movies:
        return

    insert_sql = """
    INSERT INTO movies (title, year, category, director_id, score)
    VALUES(%s, %s, %s, %s, %s)"""
    connection.cursor().executemany(insert_sql, movies)
    connection.commit()

base = declarative_base()

class Directors(base):
    __tablename__ = 'directors'

    director_id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(30), nullable=False)
    surname = Column(String(30), nullable=False)
    rating = Column('score', Integer, nullable=False)
    movies = relationship('Movies', back_populates='directors', cascade="all, delete", passive_deletes=True)

    def __repr__(self):
       return f"<Directors: director_id=%d :: name=%s :: surname=%s :: rating=%d>" \
              % ( self.director_id, self.name, self.surname, self.rating )

class Movies(base):
    __tablename__ = 'movies'

    movie_id = Column(Integer, primary_key=True, autoincrement=True)
    director_id = Column(Integer, ForeignKey('directors.director_id', ondelete="CASCADE"), nullable=False)
    title = Column(String(30), nullable=False)
    year = Column(Integer, nullable=False)
    category = Column(String(10), nullable=False)
    rating = Column('score', Integer, nullable=False)
    directors = relationship('Directors', back_populates='movies')

    def __repr__(self):
       return f"<Movies: movie_id=%d :: title=%s :: year=%d :: category=%s :: director_id=%d :: rating=%d>" \
              % ( self.movie_id, self.title, self.year, self.category, self.director_id, self.rating )

def get_directors_statistics(session, start_year, end_year):
    from sqlalchemy import column

    q = text("""
         SELECT count(movies.movie_id) AS counter, directors.director_id AS ddirectorid, 
         movies.movie_id AS mmoviesid, directors.name AS ddname, directors.surname AS ddsurname, avg(movies.score) AS average 
         FROM directors 
         JOIN movies ON directors.director_id = movies.director_id
         WHERE movies.year
         BETWEEN :start_year AND :end_year
         GROUP BY ddirectorid, mmoviesid 
         """)
    q.bindparams(
         bindparam('start_year', type_=Integer),
         bindparam('end_year', type_=Integer))\
        .columns(counter=Integer, ddirectorid=Integer, mmoviesid=Integer, ddname=String, ddsurname=String, average=Integer)

    result = session.query(column('counter'), column('ddirectorid'), column('mmoviesid'), column('ddname'), column('ddsurname'), column('average') )\
        .from_statement(q).params(start_year=start_year, end_year=end_year).all()

    return result

def delete_director(**kwargs):
    if 'name' in kwargs:
        smt = text('SELECT directors.director_id FROM directors WHERE directors.name = :name')
    elif 'surname' in kwargs:
        smt = text('SELECT directors.director_id FROM directors WHERE directors.surname = :surname')
    else:
        return
    Session = sessionmaker(bind=eng, autocommit=True)
    session = Session()
    with session.begin():
        director_id = session.query(Directors.director_id).from_statement(smt).params(**kwargs).first()

        print ( "ID of director to be deleted: %d" % director_id[0] )

        # The actual delete
        session.query(Movies).filter(Movies.director_id == director_id[0]).delete()
        session.query(Directors).filter(Directors.director_id == director_id[0]).delete()

try:

    with session:

        truncate_db( eng )
        logger.info('DB DIRECTORS Re-CREATE: ' + str(directors) + " executed.")

        insert_directors(db, directors)
        logger.info('DB OK: ' + " added)" )
        print (directors)

    cursor = db.cursor()
    cursor.execute("SELECT COUNT(*) FROM directors;")
    counter = cursor.fetchone()
    db.commit()
    print ("DB tables OK, inserted %d" % counter)

    with session:
        insert_movies(db, movies)
        logger.info('DB OK: ' + " added)")
        print (movies)

    cursor = db.cursor()
    cursor.execute("SELECT COUNT(*) FROM movies;")
    counter = cursor.fetchone()

    print ("DB tables OK, inserted %d" % counter)

    conn = eng.connect()

    # List the categories of all movies and their rankings, for the films that were
    # produced between 2011 and 2014 AND had rankings less than 9, also, sort them
    # by their ranking. Use select () and query ().
    #

    from sqlalchemy import text
    from sqlalchemy import text, bindparam

    print(get_directors_statistics(session, 1990, 2000))

    print('''  ##### SUB JOINED ##### ''')
    subq = session.query(Movies.director_id).filter( and_(Movies.year < 2001, Movies.title.like('T%'))).subquery()
    print( subq )

    Session = sessionmaker(bind=eng, autocommit=True)
    session = Session()

    with session.begin():
        subq = session.query(Directors.director_id).filter(Directors.name == 'Frank').subquery()
        print(subq)

    ### CALL a customized SQL function
    delete_director(surname='Nolan')

    ### CHECK the deletion:
    result = session.query(Directors).filter_by(director_id= 4)
    print( "\n %s" % result )
    print( "%s" % result.all() )

    conn.close()

except Exception as error:

    logger.error('DB Error: ' + str(error))
    traceback.print_exc()

    traceback.print_stack()
    print(traceback.format_exc())
    # or
    print(sys.exc_info()[2])
