import sqlalchemy.orm
from sqlalchemy.orm import relationship, Session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, String, Integer, Date, ForeignKey, column

import logging

eng = sqlalchemy.create_engine('mysql://root:1My_sql!SQL@localhost:3306/cinematic')
print(eng)
logging.info( eng )

base = declarative_base()
conn = eng.connect()

Session = sessionmaker(bind = eng)

class Directors(base):
    __tablename__ = 'directors'

    director_id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(30), nullable=False)
    surname = Column(String(30), nullable=False)
    rating = Column('score', Integer, nullable=False)
    movies = relationship('Movies', back_populates='directors', cascade="all, delete", passive_deletes=True)

    def __repr__(self):
       return f"<Directors: director_id=%d :: name=%s :: surname=%s :: rating=%d>" \
              % ( self.director_id, self.name, self.surname, self.rating )

class Movies(base):
    __tablename__ = 'movies'

    movie_id = Column(Integer, primary_key=True, autoincrement=True)
    director_id = Column(Integer, ForeignKey('directors.director_id', ondelete="CASCADE"), nullable=False)
    title = Column(String(30), nullable=False)
    year = Column(Integer, nullable=False)
    category = Column(String(10), nullable=False)
    rating = Column('score', Integer, nullable=False)
    directors = relationship('Directors', back_populates='movies')

    def __repr__(self):
       return f"<Movies: movie_id=%d :: title=%s :: year=%d :: category=%s :: director_id=%d :: rating=%d>" \
              % ( self.movie_id, self.title, self.year, self.category, self.director_id, self.rating )

try:

    session = Session()

    #
    # SELECT count(movies.movie_id) AS count_1, directors.name, directors.surname, avg(movies.rating) AS avg_1
    # FROM directors JOIN movies ON directors.director_id = movies.director_id GROUP BY directors.director_id
    #
    #  Constructed by JOIN:
    #  SELECT * FROM movies m, directors d WHERE m.director_id = d.director_id ;
    #
    from sqlalchemy import and_
    from sqlalchemy import select, join, func

    j = join(Directors, Movies)
    select = select([func.count(Movies.movie_id), Directors.name,
                     Directors.surname, func.avg(Movies.rating)]).select_from( j ).group_by(Directors.director_id)

    print ('''  ##### TASK2 ##### ''')
    result = conn.execute(select)

    print ('''  ##### SQL ##### ''')
    print( select )

    print ('''  ##### JOINED ##### ''')
    print(result.fetchall())

    print ('''  \n##### TASK3 ##### ''')
    from sqlalchemy import select, and_, between, join, func

    j = join(Directors, Movies)
    select = select([func.count(Movies.movie_id), Directors.name, Directors.surname,
                     func.avg(Movies.rating)]).select_from( j ).group_by(Directors.director_id)

    result = conn.execute(select)
    print ('''  ##### SQL ##### ''')
    print( select )

    print ('''  ##### JOINED ##### ''')
    print(result.fetchall())

    # SELECT count(movies.movie_id) AS count_1, directors.name, directors.surname, avg(movies.rating) AS avg_1
    # FROM directors JOIN movies ON directors.director_id = movies.director_id GROUP BY directors.director_id
    #
    #  Constructed by SELECT with params.
    #

    from sqlalchemy import text
    print ('''  \n##### TASK4 ##### ''')

    q = text(
        'SELECT count(movies.movie_id) AS count_1, directors.name, directors.surname, avg(movies.score) AS avg_1 FROM '
        'directors '
        'JOIN movies ON directors.director_id = movies.director_id '
        'WHERE movies.year '
        'BETWEEN :start_year AND :end_year '
        'GROUP BY directors.director_id')
    result = session.query( column('count_1'), column('name'), column('surname'), column('avg_1') ).from_statement( q )\
        .params(start_year=1950, end_year=2000).all()

    print ('''  ##### SQL ##### ''')
    print( q )

    print ('''  ##### JOINED ##### ''')
    print(result)

    from sqlalchemy import text, bindparam

    print ('''  \n##### TASK4 ##### ''')
    start_year = 2011
    end_year = 2022

    q = text("""
          SELECT count(movies.movie_id) AS counter, directors.director_id AS ddirectorid, 
          movies.movie_id AS mmoviesid, directors.name AS ddname, directors.surname AS ddsurname, avg(movies.score) AS average 
          FROM directors 
          JOIN movies ON directors.director_id = movies.director_id
          WHERE movies.year
          BETWEEN :start_year AND :end_year
          GROUP BY ddirectorid, mmoviesid 
          """)
    q.bindparams(
        bindparam('start_year', type_=Integer),
        bindparam('end_year', type_=Integer)) \
        .columns(counter=Integer, ddirectorid=Integer, mmoviesid=Integer, ddname=String, ddsurname=String,
                 average=Integer)

    print ('''  ##### SQL ##### ''')
    print( q )

    result = session.query(column('counter'), column('ddirectorid'), column('mmoviesid'),
                           column('ddname'), column('ddsurname'), column('average') )\
        .from_statement(q).params(start_year=start_year, end_year=end_year).all()

    print(result)
    conn.close()

except Exception as e:

    print (e);