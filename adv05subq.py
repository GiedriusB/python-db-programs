import sqlalchemy.orm
from sqlalchemy.orm import relationship, Session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, String, Integer, Date, ForeignKey, column

import logging

eng = sqlalchemy.create_engine('mysql://root:1My_sql!SQL@localhost:3306/cinematic')
print(eng)
logging.info( eng )

base = declarative_base()
conn = eng.connect()

Session = sessionmaker(bind = eng)

class Directors(base):
    __tablename__ = 'directors'

    director_id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(30), nullable=False)
    surname = Column(String(30), nullable=False)
    rating = Column('score', Integer, nullable=False)
    movies = relationship('Movies', back_populates='directors', cascade="all, delete", passive_deletes=True)

    def __repr__(self):
       return f"<Directors: director_id=%d :: name=%s :: surname=%s :: rating=%d>" \
              % ( self.director_id, self.name, self.surname, self.rating )

class Movies(base):
    __tablename__ = 'movies'

    movie_id = Column(Integer, primary_key=True, autoincrement=True)
    director_id = Column(Integer, ForeignKey('directors.director_id', ondelete="CASCADE"), nullable=False)
    title = Column(String(30), nullable=False)
    year = Column(Integer, nullable=False)
    category = Column(String(10), nullable=False)
    rating = Column('score', Integer, nullable=False)
    directors = relationship('Directors', back_populates='movies')

    def __repr__(self):
       return f"<Movies: movie_id=%d :: title=%s :: year=%d :: category=%s :: director_id=%d :: rating=%d>" \
              % ( self.movie_id, self.title, self.year, self.category, self.director_id, self.rating )

try:

    session = Session()

    #### MAKE a SUB-QUERY ####
    #
    # SELECT movies.director_id FROM movies
    # WHERE movies.year < :year_1 AND movies.title LIKE :title_1
    #
    from sqlalchemy import and_
    from sqlalchemy import select, join, func

    print('''  ##### TASK7 ##### ''')
    subq = session.query(Movies.director_id, Movies.title).filter( and_(Movies.year < 2001, Movies.title.like('T%')) )

    print( subq )
    for movie in subq.all():
        print( "%s" % movie )

    ## Note, we limit the in_() to first item in the list, that is ID
    session.query(Directors).filter(Directors.director_id.in_(subq[0] )).update(
        {'rating': (Directors.rating + 1)}, synchronize_session='fetch')

    session.commit()

    ### CHECK the update:
    result = session.query(Directors)
    print( "\n %s" % result )

    ## NOTE that the Coppola and Darabont ratings increased, updated.
    # Why? Because the movies they directed start with 'T' as in 'The... ' AND they are earlier than 2001
    for director in result.all():
        print( "%s" % director )

    conn.close()

except Exception as e:

    print (e);